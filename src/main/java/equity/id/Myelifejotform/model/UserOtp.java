package equity.id.Myelifejotform.model;

import equity.id.Myelifejotform.auditable.Auditable;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

@Data
@NoArgsConstructor
@Entity
@Table(name = "m_otp")
public class UserOtp extends Auditable<String> implements Serializable {

    @Column(length = 4, name = "state_code")
    private String email;

    @Column(length = 60, name = "one_time_password")
    private String oneTimePassword;

    @Column(name = "otp_requested_time")
    private Date otpRequestedTime;
}
