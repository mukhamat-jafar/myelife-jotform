package equity.id.Myelifejotform.retrofit;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;


public interface EditApi {

    @POST("eKPnzL9V22FeYYRdXb73")
    Call<EditResponse> postToEdit(@Body EditRequest request);
}
