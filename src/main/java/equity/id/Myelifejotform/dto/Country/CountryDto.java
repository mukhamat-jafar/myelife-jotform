package equity.id.Myelifejotform.dto.Country;

import lombok.Data;

import javax.persistence.Column;

@Data
public class CountryDto {

    private Long id;
    private String countryCode;
    private String description;
    private boolean isActive;
}
