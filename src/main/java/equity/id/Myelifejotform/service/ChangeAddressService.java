package equity.id.Myelifejotform.service;


import equity.id.Myelifejotform.config.error.NotFoundException;
import equity.id.Myelifejotform.dto.ChangeAddress.CreateChangeAddressDto;
import equity.id.Myelifejotform.dto.ChangeAddress.ChangeAddressDto;
import equity.id.Myelifejotform.model.ChangeAddress;
import equity.id.Myelifejotform.repository.ChangeAddressRepository;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.List;

@Service
public class ChangeAddressService {

    @Autowired
    private ChangeAddressRepository changeAddressRepository;

    @Autowired
    private ModelMapper modelMapper;

    //Get All Data Change Address
    public ResponseEntity<List<ChangeAddressDto>> listChangeAddress(){
        List<ChangeAddress> changeAddresss = changeAddressRepository.findAll();
        Type targetType = new TypeToken<List<ChangeAddress>>(){}.getType();
        List<ChangeAddressDto> response= modelMapper.map(changeAddresss, targetType);

        return ResponseEntity.ok(response);
    }

    //Get Data Base on ID
    public ResponseEntity<ChangeAddress> getChangeAddressById(Long id){
        ChangeAddress changeAddress = changeAddressRepository.findById(id).orElseThrow(()-> new NotFoundException("Data Change-Address ID " + id + " is not exist"));
        ChangeAddress response = modelMapper.map(changeAddress, ChangeAddress.class);

        return ResponseEntity.ok(response);
    }

    //Post Data Change Address
    public ResponseEntity<ChangeAddress> addChangeAddress(MultipartFile image, ChangeAddress newChangeAddress) throws IOException {

        String message = "Image only PNG & JPG";

        String jpgMimeType = "image/jpeg", pngMimeType = "image/png";
        if (!image.getContentType().equals(jpgMimeType)
                && !image.getContentType().equals(pngMimeType)){
            return new ResponseEntity<>(HttpStatus.valueOf(message));
        }

        ChangeAddress changeAddress = new ChangeAddress();

        changeAddress.setChangeAddressCode(newChangeAddress.getChangeAddressCode());
        changeAddress.setPolicyNo(newChangeAddress.getPolicyNo());
        changeAddress.setPolicyHolder(newChangeAddress.getPolicyHolder());
        changeAddress.setHandphoneNo(newChangeAddress.getHandphoneNo());
        changeAddress.setEmailAddress(newChangeAddress.getEmailAddress());
        changeAddress.setChangeData(newChangeAddress.isChangeData());
        changeAddress.setNewHandphoneNo(newChangeAddress.getNewHandphoneNo());
        changeAddress.setWhatsappNumber(newChangeAddress.isWhatsappNumber());
        changeAddress.setWhatsappNo(newChangeAddress.getWhatsappNo());
        changeAddress.setNewEmailAddress(newChangeAddress.getNewEmailAddress());
        changeAddress.setChangeAddress(newChangeAddress.isChangeAddress());
        changeAddress.setAddress1(newChangeAddress.getAddress1());
        changeAddress.setAddress2(newChangeAddress.getAddress2());
        changeAddress.setAddress3(newChangeAddress.getAddress3());
        changeAddress.setCityCode(newChangeAddress.getCityCode());

        changeAddress.setStateCode(newChangeAddress.getStateCode());
        changeAddress.setPostalCode(newChangeAddress.getPostalCode());
        changeAddress.setCountryCode(newChangeAddress.getCountryCode());
        changeAddress.setFaxNo(newChangeAddress.getFaxNo());
        changeAddress.setPhoneNo(newChangeAddress.getPhoneNo());
        changeAddress.setImage(image.getOriginalFilename());
        changeAddress.setTransactionCode(newChangeAddress.getTransactionCode());
        changeAddress.setTransactionStatus(newChangeAddress.getTransactionStatus());
        changeAddress.setActive(newChangeAddress.isActive());

        ChangeAddress response = changeAddressRepository.save(changeAddress);

        return ResponseEntity.ok(response);
    }

    //Update Data Change Address
    public ResponseEntity<ChangeAddress> editChangeAddress(ChangeAddress updateChangeAddress, Long id){
        ChangeAddress changeAddress = changeAddressRepository.findById(id).orElseThrow(()-> new NotFoundException("Change-Address ID " + id + " is not exist"));

        changeAddress.setChangeAddressCode(updateChangeAddress.getChangeAddressCode());
        changeAddress.setPolicyNo(updateChangeAddress.getPolicyNo());
        changeAddress.setPolicyHolder(updateChangeAddress.getPolicyHolder());
        changeAddress.setHandphoneNo(updateChangeAddress.getHandphoneNo());
        changeAddress.setEmailAddress(updateChangeAddress.getEmailAddress());
        changeAddress.setChangeData(updateChangeAddress.isChangeData());
        changeAddress.setNewHandphoneNo(updateChangeAddress.getNewHandphoneNo());
        changeAddress.setWhatsappNumber(updateChangeAddress.isWhatsappNumber());
        changeAddress.setWhatsappNo(updateChangeAddress.getWhatsappNo());
        changeAddress.setNewEmailAddress(updateChangeAddress.getNewEmailAddress());
        changeAddress.setChangeAddress(updateChangeAddress.isChangeData());
        changeAddress.setAddress1(updateChangeAddress.getAddress1());
        changeAddress.setAddress2(updateChangeAddress.getAddress2());
        changeAddress.setAddress3(updateChangeAddress.getAddress3());
        changeAddress.setCityCode(updateChangeAddress.getCityCode());

        changeAddress.setStateCode(updateChangeAddress.getStateCode());
        changeAddress.setPostalCode(updateChangeAddress.getPostalCode());
        changeAddress.setCountryCode(updateChangeAddress.getCountryCode());
        changeAddress.setFaxNo(updateChangeAddress.getFaxNo());
        changeAddress.setPhoneNo(updateChangeAddress.getPhoneNo());
        changeAddress.setImage(updateChangeAddress.getImage());
        changeAddress.setTransactionCode(updateChangeAddress.getTransactionCode());
        changeAddress.setTransactionStatus(updateChangeAddress.getTransactionStatus());
        changeAddress.setActive(updateChangeAddress.isActive());

        changeAddressRepository.save(changeAddress);

        ChangeAddress response = modelMapper.map(changeAddress, ChangeAddress.class);
        return ResponseEntity.ok(response);
    }


    //Remove Data Change Address
    public ResponseEntity<ChangeAddress> deleteChangeAddress(Long id){
        ChangeAddress changeAddress = changeAddressRepository.findById(id).orElseThrow(()-> new NotFoundException("Data Change-Address ID " + id + " is not exist"));
        changeAddressRepository.deleteById(id);
        ChangeAddress response = modelMapper.map(changeAddress, ChangeAddress.class);

        return ResponseEntity.ok(response);
    }
}
