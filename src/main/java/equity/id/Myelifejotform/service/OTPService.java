package equity.id.Myelifejotform.service;

import equity.id.Myelifejotform.model.UserOtp;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.text.DecimalFormat;
import java.util.Random;

@Service
public class OTPService {

    public String generateOTP(){
        var otp = new DecimalFormat("000000").format(new Random().nextInt(999999));
        return otp;
    }
}
