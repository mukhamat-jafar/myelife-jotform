package equity.id.Myelifejotform;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@SpringBootApplication
@EnableJpaRepositories("equity.id.Myelifejotform.repository")
public class MyelifeJotformApplication {

	public static void main(String[] args) {
		SpringApplication.run(MyelifeJotformApplication.class, args);
	}

}
