package equity.id.Myelifejotform.controller;

import equity.id.Myelifejotform.config.response.BaseResponse;
import equity.id.Myelifejotform.dto.ChangeAddress.CreateChangeAddressDto;
import equity.id.Myelifejotform.dto.ChangeAddress.ChangeAddressDto;
import equity.id.Myelifejotform.model.ChangeAddress;
import equity.id.Myelifejotform.retrofit.EditResponse;
import equity.id.Myelifejotform.retrofit.EditService;
import equity.id.Myelifejotform.service.ChangeAddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("api/v1/change-address")
public class ChangeAddressController {

    @Autowired
    private EditService editService;

    @Autowired
    private ChangeAddressService changeAddressService;

    @GetMapping()
    public ResponseEntity<List<ChangeAddressDto>> listChangeAddress(){
        return changeAddressService.listChangeAddress();
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @GetMapping("/{id}")
    public ResponseEntity<ChangeAddress> getChangeAddressById(@PathVariable Long id){
        return changeAddressService.getChangeAddressById(id);
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @PostMapping
    public ResponseEntity<ChangeAddress> addChangeAddress(@RequestParam("file")MultipartFile image, ChangeAddress newChangeAddress) throws IOException {
        ResponseEntity<ChangeAddress> response = changeAddressService.addChangeAddress(image, newChangeAddress);
        try {
            if(response != null){
                EditResponse editResponse = editService.postChangeAddress(image, newChangeAddress);
            }
        } catch (IOException e) {
            e.getMessage();
        }

        return changeAddressService.addChangeAddress(image, newChangeAddress);
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @PutMapping("{id}")
    public ResponseEntity<ChangeAddress> editChangeAddress(@RequestBody ChangeAddress updateChangeAddress, @PathVariable Long id){
        return changeAddressService.editChangeAddress(updateChangeAddress, id);
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @DeleteMapping("{id}")
    public ResponseEntity<ChangeAddress> deleteChangeAddress(@PathVariable Long id){
        return changeAddressService.deleteChangeAddress(id);
    }
}
