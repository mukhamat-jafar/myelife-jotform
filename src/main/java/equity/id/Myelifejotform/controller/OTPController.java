package equity.id.Myelifejotform.controller;

import equity.id.Myelifejotform.model.UserOtp;
import equity.id.Myelifejotform.service.OTPService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

@RestController
@RequestMapping("api/v1")
public class OTPController {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private OTPService otpService;

    @Autowired
    private JavaMailSender sender;

    @GetMapping("/generate-otp")
    public ResponseEntity<String> listOTP() throws MessagingException{

        MimeMessage message = sender.createMimeMessage();
        var otp = otpService.generateOTP();

        try {
            MimeMessageHelper helper = new MimeMessageHelper(message, false, "UTF-8");
            String htmMsg = "<p>Dear Nasabah, " + "</p>"
                    + "<p>Your One Time Password (OTP) code is: "
                    + "<p><b>" + otp + "</p></b>"
                    + "<br>" + "<br>"
                    + "<p>Note: This OTP is set to expire in 5 minutes.</p>";

            message.setContent(htmMsg, "text/html");
            helper.setTo("jafar.mukhamat7@gmail.com");
            helper.setSubject("OTP-Verification");

            sender.send(message);


        } catch (MessagingException e) {
            e.printStackTrace();
        }
        return ResponseEntity.ok(otp);
    }
}
